# Boolaravel

Ambiente Laravel dockerizzato.

| Container | Versione Immagine|
| ------ | ------ |
| MariaDB | latest |
| Phpmyadmin | latest |
| Laravel | (custom) php 7.4 |
| Npm | (custom) node 12 |

## Requisiti mini
Docker Engine & Docker Compose installati sulla macchina. 
Per gli utenti Windows e Mac l'installazione del [Docker Desktop](https://www.docker.com/get-started) comprende già tutto.

## Utilizzo
E' sufficiente utilizzare il comando `docker-compose up` nella cartella dove è presente il file **docker-compose.yaml** per avviare i servizi.
La prima esecuzione richiederà più tempo, le immagini verrano scaricate dal Docker Hub e verranno creati i containers.

### Crezione di un nuovo progetto laravel
Una volta avviati i servizi basterà entrare nel container laravel tramite il comando `docker-compose exec laravel bash` ed eseguire `composer create-project laravel/laravel .` verrà scaricato automaticamente laravel nella cartella **project**. 
Un database con nome "laravel" verrà automaticamente creato in fase di creazione del container "mariadb". 
Quindi basta verificare che:
- la variabile DB_HOST nel .env sia uguale al nome del servizio mariadb (mariadb)
- la variabile DB_PASSWORD nel .env sia uguale a quella impostata nel docker-compose.yaml (root)

### Laravel
Laravel è esposto alla porta 80, quindi è raggiungibile all'url http://localhost, la document root punta alla cartella **project/public**.
### PhpMyAdmin
PhpMyAdmin è esposto alla porta 8080, quindi è raggiungibile all'url http://localhost:8080
- **username**: root
- **password**: root (variabile d'ambiente presente nel docker-compose file: MYSQL_ROOT_PASSWORD)
### Npm
E' presente un container di node per utilizzare npm `docker-compose run npm bash` la working directory è project.
## Comandi utili
Per avviare i containers
```sh
$ docker-compose up
```
Per stoppare i containers
```sh
$ docker-compose down
```
Per runnare un container ed attaccare un terminale bash.
```sh
$ docker-compose run npm bash
```
Per attaccare un terminale bash ad un container già in esecuzione.
```sh
$ docker-compose exec php-apache bash
```

## Immagini Custom
Nella cartella **.docker** sono presenti le immagini docker custom per i container laravel e node.
### Laravel
Partendo dall'immagine base di **php:7.4-apache**.
- vengono aggiunti i pacchetti e le estensioni php per il corretto funzionamento dell'applicativo
- vengono copiati i file *apache2.conf* e *vh.conf*
- viene installato composer nella sua ultima versione
- viene aggiunto un nuovo utente e gruppo "laravel"
- viene impostato "laravel" come utente, per evitare problemi di permessi quando vengono creati i file.
- viene runnato apache come sudo
### Node
Partendo dall'immagine base di **node:12** viene impostato l'utente "node" per evitare problemi di permessi quando vengono creati i file.
